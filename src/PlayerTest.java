import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.util.PriorityQueue;
import java.util.Queue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.function.Executable;

class PlayerTest {

    private Player p;
    private Player p2;
    private Player p3;
    private Player p4;
    private static String[] friends;
    private PriorityQueue<String> games;

    @BeforeEach
    public void setUpBeforeClass() throws Exception {
        p = new Player("Aaron", 2, new String[]{"Kent Spencer", "Spencer Kent", "Donald Trump"});
        p2 = new Player("AaronD", 1, new String[]{"OatR", "LukeE", "Mike Pence"});
        p3 = new Player("Nathan", 3);
        p4 =  null;


        friends = new String[10];
        for(int i = 0; i < 10; i++){
            friends[i] = "Friend" + i;
        }

        games = new PriorityQueue<String>();
        for(int i = 0; i < 10; i ++){
            games.add("Game" + i);
        }
    }

    @Test
    void testPlayerNew() {
        Player temp = new Player("Aaron", 2, new String[]{"Kent Spencer", "Spencer Kent", "Donald Trump"});
        assertNotSame(p, temp);
        assertNotSame(p, p3);
        assertSame(p, p);
    }
    @Test
    void testPlayerNull() {
        assertNull(p4);
        assertNotNull(p3);

        p4 = new Player("Alex");
        assertNotNull(p4);
    }
    @Test
    void testPlayerName() {
        assertEquals("Aaron", p.getName());
        assertEquals("AaronD", p2.getName());
        assertEquals("Nathan", p3.getName());
    }
    @Test
    void testPlayerNameException() {
        assertThrows(NullPointerException.class, new Executable() {

            public void execute() throws Throwable {
                p.setName(null);
            }
        });
    }

    @Test
    void testGetName(){
        assertEquals(p.getName(), "Aaron");
        assertEquals(p2.getName(), "AaronD");
    }

    @Test
    void testSetName() {
        assertEquals(p.getName(), "Aaron");
        assertEquals(p2.getName(), "AaronD");

        p.setName("CoolKid2");
        assertTrue(p.getName() == "CoolKid2");
        p2.setName("JamesBond");
        assertEquals(p2.getName(), "JamesBond");
    }

    @Test
    void testGetNameException(){
        assertThrows(NullPointerException.class, new Executable() {
            public void execute() throws Throwable {
                p.setName(null);
            }
        });
    }

    @Test
    void testGetRank() {
        assertEquals(p.getRank(), 2);

        int rankp2 = p2.getRank();
        assertEquals(rankp2, 1);
    }

    @Test
    void testSetRank() {
        assertEquals(p.getRank(), 2);
        p.setRank(600);
        assertEquals(p.getRank(), 600);
    }

    @Test
    void testGetFriendsList() {
        assertFalse(p.getFriendsList().length == 0);
        assertEquals(p.getFriendsList()[0], "Kent Spencer");
        assertEquals(p.getFriendsList()[1], "Spencer Kent");
        assertEquals(p.getFriendsList()[2], "Donald Trump");
        assertEquals(p2.getFriendsList()[0], "OatR");
        assertEquals(p2.getFriendsList()[1], "LukeE");
        assertEquals(p2.getFriendsList()[2], "Mike Pence");
    }

    @Test
    void testSetFriendsList() {
        assertEquals(p.getFriendsList()[0], "Kent Spencer");
        assertEquals(p.getFriendsList()[1], "Spencer Kent");
        assertEquals(p.getFriendsList()[2], "Donald Trump");

        friends = new String[10];
        for(int i = 0; i < 10; i++){
            friends[i] = "Friend" + i;
        }
        p.setFriendsList(friends);
        for(int i = 0; i < 10; i++){
            assertEquals(p.getFriendsList()[i], "Friend" + i);
        }
    }

    @Test
    void testSetFriendsListException(){
        assertThrows(NullPointerException.class, new Executable() {
            public void execute() throws Throwable {
                p3.setFriendsList(null);
            }
        });
    }

    @Test
    void testAddToDownloads() {
        assertTrue(p.addToDownloads("Pokemon Gold"));
        assertTrue(p.addToDownloads("Elmo Dance Party 3"));

        assertFalse(p.getDownloads().isEmpty());

        assertEquals(p.getDownloads().peek(), "Elmo Dance Party 3");

        assertFalse(p.addToDownloads(null));
    }

    @Test
    void testGetDownloads() {
        assertTrue(p.getDownloads().isEmpty());

        assertTrue(p.addToDownloads("Pokemon Gold"));
        assertTrue(p.addToDownloads("Elmo Dance Party 3"));

        assertFalse(p.getDownloads().isEmpty());

        assertEquals(p.getDownloads().peek(), "Elmo Dance Party 3");
    }

    @Test
    void testSetDownloadQueue() {
        assertNotNull(p.getDownloads());
        assertTrue(p.getDownloads().isEmpty());

        p.setDownloadQueue(games);
        Queue<String> gamesTemp = p.getDownloads();

        for (int i = 0; i < 10; i++) {
            assertEquals(gamesTemp.poll(), "Game" + i);
        }
    }
    @Test
    void testSetDownloadQueueException(){
        assertThrows(NullPointerException.class, new Executable() {
            public void execute() throws Throwable {
                p3.setDownloadQueue(null);
            }
        });
    }

    @Test
    void testAddToFriendsList(){
        String newFriend = "Jason";
        assertTrue(p3.addToFriendsList(newFriend));

        assertTrue(findFriend(newFriend));
        assertFalse(findFriend("Imaginary"));

        assertFalse(p3.addToFriendsList(newFriend));

        for(int i = 0; i < 15; i++){
            assertTrue(p3.addToFriendsList("Friend" + i));
        }
    }

    @Test
    void testAddToFriendsListException(){
        assertThrows(NullPointerException.class, new Executable() {
            public void execute() throws Throwable {
                p2.addToFriendsList(null);
            }
        });
    }

    // Helper method to add friends
    private boolean findFriend(String friend) {
        for(String frnd : p3.getFriendsList()){
            if(frnd != null)
                if(frnd.equalsIgnoreCase(friend)){
                    return true;
                }
        }
        return false;
    }

    @Test
    void testRemoveFromFriendsList(){
        assertTrue(p.addToFriendsList("Jason"));

        //assertTrue(
        p.removeFromFriendsList("Jason");

        for(int i = 0; i < 15; i++){
            assertTrue(p3.addToFriendsList("Friend" + i));
        }
        for(int i = 0; i < 15; i++){
            //assertTrue(
            p3.removeFromFriendsList("Friend" + i);
        }
    }
}
